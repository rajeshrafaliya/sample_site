var express = require('express'),
	path = require('path'),
    app = express();
	
	
	app.use(express.static('./dist/'));

	app.get('/*', function(req, res) {
	  res.sendFile(__dirname + '/dist/index.html')
	});

	var server = app.listen(3000);
	console.log('Express server listening on rajesh port 3000');